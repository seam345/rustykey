use crate::BaseArguments;
use inquire::{Confirm, Password, PasswordDisplayMode};
use log::trace;
use rustykey::cryptsetup_caller::CrypsetupError;
use rustykey::key_helpers::{
    generate_salt_hex, save_salt_iter_to_file, KeyGenerator, SaltIter, SaltIterSaveError,
};
use rustykey::{cryptsetup_caller, yubikey};

pub enum AddError {
    AddKey(CrypsetupError),
    KeyGenerationYubikey(yubikey::Error),
    SaveKeyError(SaltIterSaveError),
}

impl From<CrypsetupError> for AddError {
    fn from(value: CrypsetupError) -> Self {
        AddError::AddKey(value)
    }
}

impl From<yubikey::Error> for AddError {
    fn from(value: yubikey::Error) -> Self {
        AddError::KeyGenerationYubikey(value)
    }
}

pub fn add_luks_key(base_args: &BaseArguments) -> Result<(), AddError> {
    let salt = generate_salt_hex(base_args.salt_length);
    let iterations = 1000;

    let mut key_gen = KeyGenerator::new_from_iterations(iterations)
        .add_salt_hex(salt.clone())
        .hash_salt()
        .challenge_yubikey(base_args.yubikey_slot.clone().into())
        .unwrap();

    match save_salt_iter_to_file(
        &base_args.salt_iter_file,
        &SaltIter {
            salt: salt.clone(),
            iterations,
        },
    ) {
        Ok(_) => {}
        Err(err) => {
            let continue_without_save = Confirm::new(&format!("Unable to save salt_iter file in {:?}, would you like to continue and save salt_iter manually", &base_args.salt_iter_file)).prompt();
            match continue_without_save {
                Ok(_) => {
                    println!("Save the following salt/iterations to a file to allow unlocking\n{salt}\n{iterations}");
                }
                Err(_) => {
                    println!("Exiting...");
                    return Err(AddError::SaveKeyError(err));
                }
            }
        }
    }

    if !base_args.one_f_a {
        let input = Password::new("Please enter new password:")
            .with_display_toggle_enabled()
            .with_display_mode(PasswordDisplayMode::Hidden)
            .with_formatter(&|_| String::from("Input received"))
            .prompt();

        let user_password = match input {
            Ok(val) => val,
            Err(_) => todo!(),
        };
        key_gen = key_gen.add_user_input(user_password);
    }

    let key1 = key_gen.derive_key(base_args.key_length);
    trace!("key generated: {:02x?}", key1);

    let current_input = Password::new("Please enter new password:")
        .with_display_toggle_enabled()
        .with_display_mode(PasswordDisplayMode::Hidden)
        .with_formatter(&|_| String::from("Input received"))
        .prompt();

    let current_password = match current_input {
        Ok(val) => val,
        Err(_) => panic!("Unable to get current password to add key"),
    };

    cryptsetup_caller::add_key(
        current_password,
        key1.as_bytes(),
        &base_args.cryptsetup,
        &base_args.device,
    )?;

    Ok(())
}
