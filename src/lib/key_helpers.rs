//! Methods to help generate a luks key

use crate::lib::key_helpers::Key::{SaltHexString, Unset};
use crate::lib::key_helpers::SaltIterRetrievalError::{
    IterationsNotFound, IterationsWrongType, ReadFileError, SaltNotFound,
};
use crate::yubikey;
use crate::yubikey::chal_resp_yubikey;
use log::{debug, error, trace};
use pbkdf2::pbkdf2_hmac;
use rand::{thread_rng, Rng};
use sha2::{Digest, Sha512};
use std::fs::File;
use std::io::Error;
use std::io::Write;
use std::path::PathBuf;
use std::{fs, io};
use yubico_manager::config::Slot;

#[derive(PartialEq, Debug)]
pub struct KeyGenerator {
    iterations: u32,
    user_input: Option<String>,
    key: Key,
}

#[derive(PartialEq, Debug)]
pub enum Key {
    Unset,
    SaltHexString(String),
    SaltDigested([u8; 64]), // should've called it salt digest but digested was just to fun :D
    YubikeyResponse([u8; 20]),
}

// todo reimplement these into different types not enums, this will allow it to be a compile error instead of a runtime error
impl KeyGenerator {
    pub fn new_from_salt_iter_file(
        salt_iter_file: &PathBuf,
    ) -> Result<Self, SaltIterRetrievalError> {
        let salt_iter = get_salt_and_iteration_from_file(salt_iter_file)?;
        Ok(KeyGenerator {
            iterations: salt_iter.iterations,
            user_input: None,
            key: SaltHexString(salt_iter.salt),
        })
    }

    pub fn new_from_iterations(iteration_count: u32) -> Self {
        KeyGenerator {
            iterations: iteration_count,
            user_input: None,
            key: Unset,
        }
    }

    pub fn add_salt_hex(self, salt_hex: String) -> Self {
        KeyGenerator {
            iterations: self.iterations,
            user_input: self.user_input,
            key: SaltHexString(salt_hex),
        }
    }

    pub fn add_user_input(self, user_input: String) -> Self {
        KeyGenerator {
            iterations: self.iterations,
            user_input: Some(user_input),
            key: self.key,
        }
    }

    pub fn hash_salt(self) -> Self {
        match self.key {
            SaltHexString(salt) => {
                let mut hasher = Sha512::new();
                hasher.update(salt.as_str());
                let result = hasher.finalize();

                KeyGenerator {
                    iterations: self.iterations,
                    user_input: self.user_input,
                    key: Key::SaltDigested(result.into()),
                }
            }
            _ => {
                error!("cannot be called in this state");
                todo!()
            }
        }
    }

    pub fn challenge_yubikey(self, slot: Slot) -> Result<Self, yubikey::Error> {
        match self.key {
            Key::SaltDigested(byte_array) => {
                let response = chal_resp_yubikey(&byte_array, slot)?;
                Ok(KeyGenerator {
                    iterations: self.iterations,
                    user_input: self.user_input,
                    key: Key::YubikeyResponse(response.0),
                })
            }
            _ => {
                error!("cannot be called in this state, maybe make a new error");
                todo!()
            }
        }
    }
    pub fn derive_key(self, length: usize) -> String {
        match self.key {
            Key::YubikeyResponse(hmac) => {
                let mut key1: Vec<u8> = vec![0; length];

                match self.user_input {
                    None => {
                        pbkdf2_hmac::<Sha512>("".as_bytes(), &hmac, self.iterations, &mut key1);
                        hex::encode(key1)
                    }
                    Some(user_input) => {
                        pbkdf2_hmac::<Sha512>(
                            user_input.trim().as_bytes(),
                            &hmac,
                            self.iterations,
                            &mut key1,
                        );

                        hex::encode(key1)
                    }
                }
            }
            _ => {
                error!("cannot be called in this state, maybe make a new error");
                todo!()
            }
        }
    }
}

#[derive(Debug)]
pub struct SaltIter {
    pub salt: String,
    pub iterations: u32,
}

#[derive(Debug)]
pub enum SaltIterRetrievalError {
    SaltNotFound,
    IterationsNotFound,
    IterationsWrongType,
    ReadFileError(Error),
}

impl From<Error> for SaltIterRetrievalError {
    fn from(value: Error) -> Self {
        ReadFileError(value)
    }
}

#[derive(Debug)]
pub enum SaltIterSaveError {
    WriteFileError(Error),
    CreateFileError(Error),
}
fn to_write_salt_error(io_error: io::Error) -> SaltIterSaveError {
    SaltIterSaveError::WriteFileError(io_error)
}
fn to_create_salt_error(io_error: io::Error) -> SaltIterSaveError {
    SaltIterSaveError::CreateFileError(io_error)
}

impl From<SaltIterSaveError> for io::Error {
    fn from(val: SaltIterSaveError) -> Self {
        match val {
            SaltIterSaveError::WriteFileError(error) => error,
            SaltIterSaveError::CreateFileError(error) => error,
        }
    }
}

pub fn save_salt_iter_to_file(
    path: &PathBuf,
    salt_iter: &SaltIter,
) -> Result<(), SaltIterSaveError> {
    debug!("Saving salt_iter to file");
    trace!(
        "salt: {}, iter: {}, path: {:?}",
        salt_iter.salt,
        salt_iter.iterations,
        path
    );
    let mut output = File::create(path).map_err(to_create_salt_error)?;
    write!(output, "{}\n{}", salt_iter.salt, salt_iter.iterations).map_err(to_write_salt_error)?;

    Ok(())
}

pub fn get_salt_and_iteration_from_file(
    file_location: &PathBuf,
) -> Result<SaltIter, SaltIterRetrievalError> {
    let text: String = fs::read_to_string(file_location)?;
    let mut split = text.split('\n');

    if let Some(salt) = split.next() {
        let salt = salt.to_owned();
        trace!("salt: '{salt}'");

        trace!("Get iterations from file string: {}", text);
        if let Some(iterations) = split.next() {
            trace!("Parse iterations into a u32");
            if let Ok(iterations) = iterations.parse() {
                trace!("iterations: '{iterations}'");
                Ok(SaltIter { salt, iterations })
            } else {
                error!("Iterations cannot be parsed into u32");
                Err(IterationsWrongType)
            }
        } else {
            error!("unable to find iterations in file");
            Err(IterationsNotFound)
        }
    } else {
        error!("unable to find salt in file");
        Err(SaltNotFound)
    }
    // todo move to the bellow once nixos has rust 1.65.0
    /*trace!("Get salt from file string: {}", text);
    let Some(salt) = split.next() else {
        error!("unable to find salt in file");
        return Err(SaltNotFound);
    };
    let salt = salt.to_owned();

    trace!("Get iterations from file string: {}", text);
    let Some(iterations) = split.next() else {
        error!("unable to find iterations in file");
        return Err(IterationsNotFound);
    };

    trace!("Parse iterations into a u32");
    let Ok(iterations) = iterations.parse() else {
        error!("Iterations cannot be parsed into u32");
        return Err(IterationsWrongType);
    };

    Ok(SaltIter{
        salt,
        iterations,
    })*/
}

/*// todo how do I make this testable, without yubikey
pub fn generate_key(user_input: &str, yubikey_slot: Slot, salt_iter_file: &PathBuf) -> [u8; 64]{
    let salt_iter = get_salt_and_iteration_from_file(salt_iter_file).unwrap();

    let mut hasher = Sha512::new();
    hasher.update(salt_iter.salt.as_str());
    let result = hasher.finalize();

    let response = chal_resp_yubikey(&result, yubikey_slot);
    // println!("response {:02x?}",response);

    let mut key1 = [0u8; 64];

    pbkdf2_hmac::<Sha512>(user_input.trim().as_bytes(), response.unwrap().deref(), salt_iter.iterations, &mut key1);

    key1

}
*/

pub fn generate_salt_hex(length: u8) -> String {
    // 8 should end up with a hex of length 16
    let mut salt: Vec<u8> = (0u8..length).collect();
    thread_rng().fill(&mut salt[..]);
    let mut s_salt = "".to_owned();
    for num in salt {
        s_salt += &format!("{:02x}", num);
    }
    s_salt
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;
    use std::fs::File;
    use std::io::{ErrorKind, Write};

    // This challenge was generated following https://nixos.wiki/wiki/Yubikey_based_Full_Disk_Encryption_(FDE)_on_NixOS
    #[test]
    fn test_read_empty_file() {
        let postfix_num: u64 = rand::random();

        let path = env::temp_dir().join(format!("test_read_empty_file{}", postfix_num.to_string()));

        match get_salt_and_iteration_from_file(&path) {
            Ok(_) => {
                assert!(false)
            }
            Err(error) => match error {
                ReadFileError(io_error) => match io_error.kind() {
                    ErrorKind::NotFound => {
                        assert!(true)
                    }
                    _ => {
                        assert!(false)
                    }
                },
                _ => assert!(false),
            },
        }
    }

    #[test]
    fn check_salt_hex_length() {
        assert_eq!(32, dbg!(generate_salt_hex(16)).len());
        assert_eq!(128, dbg!(generate_salt_hex(64)).len());
    }

    #[test]
    fn key_generator() {
        let salt = "0123456789abcdef".to_owned();
        let mut key_gen = KeyGenerator::new_from_iterations(1000)
            .add_salt_hex(salt)
            .add_user_input("test_password".to_owned())
            .hash_salt();

        key_gen.key = Key::YubikeyResponse([0u8; 20]);

        let expected: [u8; 64] = [
            181, 76, 51, 132, 179, 4, 191, 93, 198, 239, 246, 196, 198, 169, 30, 69, 234, 226, 242,
            173, 210, 217, 88, 41, 44, 216, 148, 28, 43, 234, 210, 252, 12, 206, 172, 207, 115,
            114, 79, 65, 66, 249, 244, 126, 97, 177, 33, 203, 7, 52, 169, 13, 28, 88, 254, 62, 14,
            58, 113, 15, 21, 237, 37, 6,
        ];
        assert_eq!(key_gen.derive_key(64), hex::encode(expected));
    }

    #[test]
    fn confirm_add_and_open_make_same_key() {
        let salt = generate_salt_hex(16);
        let iterations = 1000;
        let challenge_input = "unlock";

        let mut key_add = KeyGenerator::new_from_iterations(iterations)
            .add_salt_hex(salt.clone())
            .add_user_input(challenge_input.to_owned())
            .hash_salt();

        let postfix_num: u64 = rand::random();

        let salt_iter_path =
            env::temp_dir().join(format!("test_open_add_same{}", postfix_num.to_string()));

        let mut file = File::create(dbg!(&salt_iter_path)).unwrap();
        file.write_all(&format!("{salt}\n{iterations}").into_bytes())
            .unwrap();
        drop(file);

        let mut key_open = KeyGenerator::new_from_salt_iter_file(&salt_iter_path)
            .unwrap()
            .add_user_input(challenge_input.to_owned())
            .hash_salt();

        // todo there is a few unwraps above it may panic before being able to remove... need better solution
        fs::remove_file(&salt_iter_path).unwrap();

        assert_eq!(key_add, key_open);
        key_open.key = Key::YubikeyResponse([0u8; 20]);
        key_add.key = Key::YubikeyResponse([0u8; 20]);
        assert_eq!(key_add.derive_key(30), key_open.derive_key(30));
    }
}
