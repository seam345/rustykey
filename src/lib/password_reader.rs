use std::io;
use std::io::Read;
use std::str::Utf8Error;

#[derive(Debug)]
pub enum PasswordReadError {
    Unknown,
    InvalidChars(Utf8Error),
    IoError(io::Error),
}

impl From<io::Error> for PasswordReadError {
    fn from(value: io::Error) -> Self {
        PasswordReadError::IoError(value)
    }
}

impl From<Utf8Error> for PasswordReadError {
    fn from(value: Utf8Error) -> Self {
        PasswordReadError::InvalidChars(value)
    }
}

pub fn read_password_file<B>(readable: &mut B) -> Result<String, PasswordReadError>
where
    B: Read + ?Sized,
{
    // chose a MB as feels small enough on the assumed platforms it would run on but big enough to contain any password in 1 buf, if I learn it is used on more resource constrained devices this may change
    let mut buffer: [u8; 1024] = [0u8; 1024];
    let mut result = "".to_owned();
    'read_loop: loop {
        let read_bytes = readable.read(&mut buffer)?;
        if read_bytes == 0 {
            break 'read_loop;
        }
        result += std::str::from_utf8(&buffer[0..read_bytes])?;
    }
    Ok(result)
}
