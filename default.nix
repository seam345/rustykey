let
pkgs = import <nixpkgs> {};
in
  let
    package = { pkgs, fetchFromGitLab, stdenv, cargoSha, versionTag, ... }:
      pkgs.rustPlatform.buildRustPackage rec {
        pname = "rustykey";
        version = "${versionTag}";

#        src = builtins.filterSource
#                (path: type: type != "directory" || baseNameOf path != "target")
#                ./.;

        src = fetchFromGitLab {
            domain = "gitlab.com";
            owner = "seam345";
            repo = pname;
            rev = "2266b8e";
            sha256 = "sha256-chQOOSa8L8/AnVTOn/a/lk86nTVrzGfuVbS5R8hfvDg=";
          };
        cargoSha256 = "${cargoSha}";
        nativeBuildInputs = [
            # for block-utils, to mount the device with the salt on
            pkgs.pkg-config
        ];
        buildInputs = [
            pkgs.openssl # for sha2 digest
            # for block-utils, to mount the device with the salt on
            pkgs.systemd
        ];
      };
  in
    with pkgs; {
      rustykey =
        callPackage package {
          versionTag= "v0.40";
          cargoSha = "sha256-zz7WZmrnkHctl6x/+UMSz90SlkqErH+XFTQV12ie7Tk=";
        };
    }
